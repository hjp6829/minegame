using UnityEngine;
public interface IMoveable
{
    public void Move(Vector2 value);
    public void Jump();
}
public interface IMouseClick
{
    public void MouseLeftClick(bool value);
    public void MouseRightClick(bool value);
}
public interface IRideable
{
    public void RideCharacter(bool value);
}
public interface ISkillEquipable
{
    public void SetRightSkill(ISkill skill);
    public void ActiveRightSkill(bool value);
    public void ActiveLeftSkill(bool value);
}
public interface ISkill
{
    public void SkillButtonClickStart(Character character);
    public void SkillButtonClickEnd();
    public void RightMouseClick(bool value);
    public void LeftMouseClick(bool value);
    public void UpdateSkill();
    public void FixedUpdateSkill();
    public bool CheckSkillEnd();
}
public interface IPoolable
{
    GameObject gameObject { get; }
    public void Intialize();
    public void Active();
    public void Disabled();
}
