using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillUpdaterComponent : MonoBehaviour
{
    private Dictionary<System.Type,ISkill> skillDic=new Dictionary<System.Type, ISkill>();
    private void Start()
    {
        enabled = false;
    }

    public void PushSkill(ISkill skill)
    {
        skillDic.Add(skill.GetType(), skill);
        if(skillDic.Count>0)
            enabled = true;
    }
    public void PullSkill(ISkill skill)
    {
        skillDic.Remove(skill.GetType());
        if (skillDic.Count == 0)
            enabled = false;
    }

    private void FixedUpdate()
    {
        foreach(ISkill skill in skillDic.Values)
        {
            skill.FixedUpdateSkill();
        }
    }
    private void Update()
    { 
        List<System.Type> skillsToRemove = new List<System.Type>();

        foreach (var skill in skillDic)
        {
            if (skill.Value.CheckSkillEnd())
            {
                skillsToRemove.Add(skill.Key);
            }
            else
            {
                skill.Value.UpdateSkill();
            }
        }

        foreach (var key in skillsToRemove)
        {
            skillDic.Remove(key);
        }
    }
    private void LateUpdate()
    {
        //foreach (ISkill skill in skillDic.Values)
        //{
        //    skill.late();
        //}
    }
}
