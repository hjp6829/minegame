using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareSkill : ISkill
{
    private Character character;
    private bool isSkillEnd = true;
    private LayerMask exceptLayer;
    public bool CheckSkillEnd()
    {
        return isSkillEnd;
    }

    public void FixedUpdateSkill()
    {
       
    }

    public void LeftMouseClick(bool value)
    {
        if (!value|| isSkillEnd)
            return;
        Vector2 temp = InputManager.MousePos - character.Pos2;
        float angle = Mathf.Atan2(temp.y, temp.x);
        angle = angle * Mathf.Rad2Deg;
        ProjectileManager projectileManager = ServiceLocatorManager.GetService<ProjectileManager>();
        ProjectileBase projectile = projectileManager.GetPorjctileObject<FlareProjectile>(ProjectileType.FLARE);
        projectile.transform.position=character.Pos2;
        projectile.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));
        isSkillEnd = true;
    }

    public void RightMouseClick(bool value)
    {
       
    }

    public void SkillButtonClickEnd()
    {
     
    }

    public void SkillButtonClickStart(Character character)
    {
        exceptLayer = (1 << 6) | (1 << 7);
        exceptLayer = ~exceptLayer;
        isSkillEnd = false;
        this.character = character;
        this.character.GetComponent<SkillUpdaterComponent>().PushSkill(this);

    }

    public void UpdateSkill()
    {
        float dis = Vector2.Distance(character.Pos2, InputManager.MousePos);
        RaycastHit2D hit = Physics2D.Raycast(character.Pos2, InputManager.MousePos - (Vector2)character.transform.position, dis, exceptLayer);
        character.testline.SetPosition(0, character.Pos2);

        if (hit.collider != null)
            character.testline.SetPosition(1, hit.point);
        else
            character.testline.SetPosition(1, InputManager.MousePos);
    }
}
