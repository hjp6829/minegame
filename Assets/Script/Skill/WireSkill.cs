using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;

public class WireSkill : ISkill
{
    private bool isSkillEnd=true;
    private bool isCharacterMove;
    private Character character;
    private Vector2 moveDir;
    private Vector2 targetPos;
    private LayerMask exceptLayer;
    private bool isActive;

    public bool CheckSkillEnd()
    {
        return isSkillEnd;
    }

    public void FixedUpdateSkill()
    {
        if(isCharacterMove)
            character.characterRB.velocity = moveDir * 3;
    }

    public void LeftMouseClick(bool value)
    {
        isCharacterMove = true;
        character.characterRB.gravityScale = 0;
        character.IsCanMove = false;
        float dis = Vector2.Distance(character.Pos2, InputManager.MousePos);
        RaycastHit2D hit = Physics2D.Raycast(character.Pos2, InputManager.MousePos - character.Pos2, dis, exceptLayer);
        if (hit.collider != null)
            targetPos = hit.point;
        else
            targetPos = InputManager.MousePos;
        moveDir = (targetPos - character.Pos2).normalized;
    }

    public void RightMouseClick(bool value)
    {
       
    }

    public void SkillButtonClickEnd()
    {

    }

    public void SkillButtonClickStart(Character character)
    {
        if(isActive)
        {
            isSkillEnd = true;
            isCharacterMove = false;
            isActive = false;
            return;
        }
        isActive = true;
        isSkillEnd = false;
        isCharacterMove = false;
        this.character = character;
        this.character.GetComponent<SkillUpdaterComponent>().PushSkill(this);
        exceptLayer = (1 << 6) | (1 << 7);
        exceptLayer = ~exceptLayer;
    }

    public void UpdateSkill()
    {
        if (!isCharacterMove)
        {
            float dis=Vector2.Distance(character.Pos2, InputManager.MousePos);
            RaycastHit2D hit = Physics2D.Raycast(character.Pos2, InputManager.MousePos - (Vector2)character.transform.position, dis, exceptLayer);
            character.testline.SetPosition(0, character.Pos2);

            if(hit.collider!=null)
                character.testline.SetPosition(1, hit.point);
            else
                character.testline.SetPosition(1, InputManager.MousePos);
            return;
        }
      
        if(Vector2.Distance(targetPos, character.Pos2) <=0.6f)
        {
            character.IsCanMove = true;
            character.characterRB.gravityScale = 1;
            isSkillEnd = true;
            isActive = false;
            isCharacterMove = false;
        }
    }
}
