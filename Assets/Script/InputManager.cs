using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;

public class InputManager : MonoBehaviour
{

    public static Vector2 MousePos;
    [HideInInspector] public Action<Vector2> OnMoveAxis;
    [HideInInspector] public Action<bool> OnMouseLeft;
    [HideInInspector] public Action<bool> OnMouseRight;
    [HideInInspector] public Action OnRideVehicle;
    [HideInInspector] public Action OnJump;

    [HideInInspector] public Action<bool> OnSkillRight;
    [HideInInspector] public Action<bool> OnSkillLeft;

    [SerializeField] private Camera cam;
    private PlayerInput input;
    private Vector2 MoveAxis;
    private void Awake()
    {
        input = new PlayerInput();
        input.Player.Enable();
        input.Player.Move.performed += Content =>
        {
            MoveAxis = Content.ReadValue<Vector2>();
        };
        input.Player.Move.canceled += Content =>
        {
            MoveAxis = Content.ReadValue<Vector2>();
        };
        input.Player.MouseLeft.performed += Content =>
        {
            OnMouseLeft?.Invoke(true);
        };
        input.Player.MouseLeft.canceled += Content =>
        {
            OnMouseLeft?.Invoke(false);
        };
        input.Player.MouseRight.performed += Content =>
        {
            OnMouseRight?.Invoke(true);
        };
        input.Player.MouseRight.canceled += Content =>
        {
            OnMouseRight?.Invoke(false);
        };
        input.Player.Ride.canceled += Content =>
        {
            OnRideVehicle?.Invoke();
        };
        input.Player.Jump.performed += Content =>
        {
            OnJump?.Invoke();
        };
        #region Skill
        input.Player.RightSkill.performed += Content =>
        {
            OnSkillRight?.Invoke(true);
        };
        input.Player.RightSkill.canceled += Content =>
        {
            OnSkillRight?.Invoke(false);
        };
        input.Player.LeftSkill.performed += Content =>
        {
            OnSkillLeft?.Invoke(true);
        };
        input.Player.LeftSkill.canceled += Content =>
        {
            OnSkillLeft?.Invoke(false);
        };
        #endregion
    }
    private void Update()
    {
        MousePos = cam.ScreenToWorldPoint(Input.mousePosition);
    }
    private void FixedUpdate()
    {
        OnMoveAxis?.Invoke(MoveAxis);
    }
}
