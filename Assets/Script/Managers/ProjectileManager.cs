using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : SerializedMonoBehaviour,IService
{
    private Dictionary<ProjectileType, ProjectileBase> projectileDic = new Dictionary<ProjectileType, ProjectileBase>();
    private Dictionary<ProjectileType, PullingSystem> poolingSystem = new Dictionary<ProjectileType, PullingSystem>();
    private void Awake()
    {
        Register();
        ProjectileBase[] projectiles = Resources.LoadAll<ProjectileBase>("Projectiles");
        for(int i=0;i<projectiles.Length; i++)
        {
            projectileDic.Add(projectiles[i].ObjectType, projectiles[i]);
        }

    }
    public T GetPorjctileObject<T>(ProjectileType type)
    {
        if (!poolingSystem.ContainsKey(type))
        {
            ProjectileBase projectile = projectileDic[type];
            GameObject newobject = new GameObject();
            newobject.transform.parent = transform;
            PullingSystem tempSystem = newobject.AddComponent<PullingSystem>();
            tempSystem.SetNewObject(projectile);
            poolingSystem.Add(projectile.ObjectType, tempSystem);
        }
        return (T)poolingSystem[type].PullObject<T>();
    }

    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
}
