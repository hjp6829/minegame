using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Player : Character
{
    [SerializeField] private Camera gameCamera;
    [SerializeField] private Transform armJoint;

    private Collider2D collider;
    private void Start()
    {
        isPlayerCharacter = true;
        StartCoroutine(FindVehicle());
    }
    public override void MouseRightClick(bool value)
    {

    }
    public override void MouseLeftClick(bool value)
    {
        
    }
    public override void RideCharacter(bool value)
    {
        if (!value)
            this.gameObject.SetActive(false);
        else
        { 
            this.gameObject.SetActive(true);
            enabled = true;
        }
    }
    public override void Jump()
    {
        isJump = true;
        characterRB.AddForce(Vector2.up*500);
        Debug.Log("���� ����");
    }
    public override void Move(Vector2 value)
    {
        if(value.x > 0)
        viewDirection = transform.right;
        else if(value.x < 0)
            viewDirection = -transform.right;
        float rightFower = speed * value.x;
        Vector2 force;
        force = new Vector2(rightFower, characterRB.velocity.y);
        characterRB.velocity=force;
    }
    public Character GetRideCharacter()
    {
        vehicle temp = collider.GetComponent<vehicle>();
        temp.SetDriver(this);
        return temp;
    }
    private void Update()
    {

    }
    private void FixedUpdate()
    {
        Vector2 mousePos = gameCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 normal = (mousePos- (Vector2)transform.position).normalized;
        float angle = Mathf.Atan2(normal.y, normal.x)* Mathf.Rad2Deg;
        if (angle < 0)
            angle += 360;
        armJoint.rotation=Quaternion.Euler(new Vector3(0, 0, angle - 90));
    }
    IEnumerator FindVehicle()
    {
        while (true)
        {
            collider = Physics2D.OverlapCircle(transform.position, 3, 1 << 7);
            if (collider != null)
            {
                float temp = Vector2.Dot(viewDirection, (collider.transform.position - transform.position).normalized);
                if (temp > 0)
                {
                    isCanRideCharacter = true;
                }
            }
            else
                isCanRideCharacter = false;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
