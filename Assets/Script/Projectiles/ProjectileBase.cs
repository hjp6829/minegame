using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProjectileBase : MonoBehaviour,IPoolable
{
    public ProjectileType ObjectType { get => objectType; }

    [SerializeField] protected float speed;
    [SerializeField] protected ProjectileType objectType;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void FixedUpdate()
    {
        transform.Translate(Vector2.up * speed*Time.deltaTime);
    }

    public  void Intialize()
    {
       
    }

    public void Active()
    {
      gameObject.SetActive(true);
    }

    public void Disabled()
    {
        gameObject.SetActive(false);
    }
}
