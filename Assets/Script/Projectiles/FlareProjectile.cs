using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareProjectile : ProjectileBase
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        int temp = collision.gameObject.layer;
        if (temp == 6 || temp == 7)
            return;
        enabled = false;
    }
}
