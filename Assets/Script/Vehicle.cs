using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class vehicle : Character, ISkillEquipable
{
    [SerializeField] private Camera gameCamera;
    [SerializeField] private Transform weaponArmm;
    [SerializeField] private Transform drillArmm;
    private bool isDrillActive;
    private Quaternion defaultDrillRotation;
    private Character driver;
    private void Start()
    {
        defaultDrillRotation = drillArmm.rotation;
        InitializeSlots(Enum.GetValues(typeof(SlotType)).Cast<SlotType>());
        EquipWeapon(SlotType.LeftShoulder, new FlareSkill());
        EquipWeapon(SlotType.RightShoulder, new WireSkill());
    }
    public override void MouseRightClick(bool value)
    {
        isDrillActive = value;
        if(!value)
           drillArmm.rotation = defaultDrillRotation;
        GetWeaponSlot(SlotType.LeftShoulder).skill.RightMouseClick(value);

        //rightArmSkill.RightMouseClick(value);
    }
    public override void MouseLeftClick(bool value)
    {
        GetWeaponSlot(SlotType.LeftShoulder).skill.LeftMouseClick(value);
        //rightArmSkill.LeftMouseClick(value);
        if (isDrillActive)
            return;
    }
    public void SetDriver(Character driver)
    {
        this.driver = driver;
    }
    public override void Jump()
    {
        
    }
    public override void Move(Vector2 value)
    {
        if (!isCanMove)
            return;
        if (value.x > 0)
            viewDirection = transform.right;
        else if (value.x < 0)
            viewDirection = -transform.right;
        float rightFower = speed * value.x;
        Vector2 force;
            force = new Vector2(rightFower, characterRB.velocity.y);
        characterRB.velocity = force;
    }
    public override void RideCharacter(bool value)
    {
        enabled = value;
        isCanRideCharacter = value;
        if(!value)
        {
            driver.transform.position = (Vector2)transform.position - viewDirection * 2;
            driver = null;
        }
    }
    private void FixedUpdate()
    {
        Vector2 mousePos = gameCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 normal = (mousePos - (Vector2)transform.position).normalized;
        float temp = Vector2.Dot(normal, transform.right);
        if (temp < 0)
            return;
        float angle = Mathf.Atan2(normal.y, normal.x) * Mathf.Rad2Deg;
        if (angle < 0)
            angle += 360;
        if(!isDrillActive)
            weaponArmm.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));
        else
            drillArmm.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));
    }

    public void SetRightSkill(ISkill skill)
    {
       
    }

    public void ActiveRightSkill(bool value)
    {
        if(value)
            GetWeaponSlot(SlotType.RightShoulder).skill.SkillButtonClickStart(this);
        else
            GetWeaponSlot(SlotType.RightShoulder).skill.SkillButtonClickEnd();
    }

    public void ActiveLeftSkill(bool value)
    {
        if (value)
            GetWeaponSlot(SlotType.LeftShoulder).skill.SkillButtonClickStart(this);
        else
            GetWeaponSlot(SlotType.LeftShoulder).skill.SkillButtonClickEnd();
    }
}
