using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [SerializeField] Character player;
    [SerializeField] Character bot;
    [SerializeField] InputManager inputmanager;
    private Character defaultCharacter;
    private Character currentCharacter;
    private void Start()
    {
        defaultCharacter = player;
        SetCharacter(defaultCharacter);
        inputmanager.OnMoveAxis += (Vector2 value) =>
        {
            CharacterMove(value);
        };
        inputmanager.OnMouseLeft += (bool value) =>
        {
            CharacterMouseLeft(value);
        };
        inputmanager.OnMouseRight += (bool value) =>
        {
            CharacterMouseRight(value);
        };
        inputmanager.OnRideVehicle += () =>
        {
            CharacterRide();
        };
        inputmanager.OnJump += () =>
        {
            JumpCharacter();
        };
        inputmanager.OnSkillRight += (bool value) =>
        {
            RightSkill(value);
        };
        inputmanager.OnSkillLeft += (bool value) =>
        {
            LeftSkill(value);
        };
    }
    private void SetCharacter(Character character)
    {
        if (currentCharacter != null)
        {
            currentCharacter.RideCharacter(false);
        }
        currentCharacter = character;
        currentCharacter.RideCharacter(true);
    }
    private void CharacterMove(Vector2 value)
    {
        currentCharacter.Move(value);
    }
    private void CharacterMouseLeft(bool value)
    {
        currentCharacter.MouseLeftClick(value);
    }
    private void CharacterMouseRight(bool value)
    {
        currentCharacter.MouseRightClick(value);
    }
    private void CharacterRide()
    {
        if(currentCharacter.CheckPlayerCharacter()&& currentCharacter.CanRide())
        {
            SetCharacter((currentCharacter as Player).GetRideCharacter());
        }
        else if(!currentCharacter.CheckPlayerCharacter()&& currentCharacter.CanRide())
        {
            SetCharacter(defaultCharacter);
        }
    }
    private void JumpCharacter()
    {
        currentCharacter.Jump();
    }
    private void RightSkill(bool value)
    {
        try
        { currentCharacter.GetComponent<ISkillEquipable>().ActiveRightSkill(value); }
        catch { }
    }
    private void LeftSkill(bool value)
    {
        try
        { currentCharacter.GetComponent<ISkillEquipable>().ActiveLeftSkill(value); }
        catch { }
    }
    private void FixedUpdate()
    {
        
    }
}
