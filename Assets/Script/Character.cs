using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class WeaponSlot
{
    public bool isEnabled = false;
    public ISkill skill = null;

    public void EnableSlot() => isEnabled = true;
    public void EquipWeapon(ISkill skill)
    {
        if (isEnabled) 
            this.skill = skill;
    }
}
public abstract class Character : MonoBehaviour, IMoveable, IMouseClick, IRideable
{
    public LineRenderer testline;
    public Rigidbody2D characterRB
    {
        get
        {
            if (rb == null)
                rb = GetComponent<Rigidbody2D>();
            return rb;
        }
    }
    public bool IsCanMove { set => isCanMove = value; }
    public Vector2 Pos2 { get => (Vector2)transform.position; }
    [SerializeField]
    protected float speed;
    protected bool isCanRideCharacter;
    protected bool isPlayerCharacter;
    protected Vector2 viewDirection;
    protected bool isCanMove=true;
    protected bool isJump;
    protected bool isGroundCheck;
    protected Dictionary<SlotType, WeaponSlot> weaponSlots = new Dictionary<SlotType, WeaponSlot>();

    private Rigidbody2D rb;

    private void Awake()
    {
        enabled = false;
    }
    private void Start()
    {
        viewDirection = transform.right;
    }
   
    protected void InitializeSlots(IEnumerable<SlotType> enabledSlots)
    {
        foreach (SlotType slot in Enum.GetValues(typeof(SlotType)))
        {
            weaponSlots[slot] = new WeaponSlot();

            if (enabledSlots.Contains(slot))
            {
                weaponSlots[slot].EnableSlot();
            }
        }
    }
    public void EquipWeapon(SlotType slot, ISkill skill)
    {
        if (weaponSlots.ContainsKey(slot))
        {
            weaponSlots[slot].EquipWeapon(skill);
        }
    }
    protected WeaponSlot GetWeaponSlot(SlotType slot)
    {
       return weaponSlots[slot];
    }
    private void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 0.6f, 1 << 4);
        Debug.DrawRay(transform.position, Vector2.down * 0.6f);
        if (hit)
            isGroundCheck = true;
        else
            isGroundCheck = false;
    }
    public bool CanRide()
    {
        return isCanRideCharacter;
    }
    public bool CheckPlayerCharacter()
    {
        return isPlayerCharacter;
    }
    public abstract void MouseLeftClick(bool value);
    public abstract void MouseRightClick(bool value);
    public abstract void Move(Vector2 value);
    public abstract void RideCharacter(bool value);
    public abstract void Jump();
}
