using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullingSystem : MonoBehaviour
{
    private Queue<IPoolable> poolQueue= new Queue<IPoolable>();
    public void SetNewObject(IPoolable poolObject)
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject poolObjectInstance = Instantiate(poolObject.gameObject, transform.position, transform.rotation, transform);
            IPoolable poolInstance = poolObjectInstance.GetComponent<IPoolable>();
            if (poolInstance != null)
            {
                poolInstance.Disabled();
                poolQueue.Enqueue(poolInstance);
            }
        }
    }
   public void PushObject(IPoolable poolObject)
    {
        poolQueue.Enqueue(poolObject);
    }
    public T PullObject<T>()
    {
        IPoolable pool =  poolQueue.Dequeue();
        pool.Active();
        return (T)pool;
    }
}
