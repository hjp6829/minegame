using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript1 : MonoBehaviour
{
    [SerializeField]
    private int minOffst;
    [SerializeField]
    private int maxOffst;
    [SerializeField]
    private GameObject pixcel;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MakeMap());
    }
    IEnumerator MakeMap()
    {
        Vector2 tempPos = new Vector2(88.5f, -49.5f);
        while (true)
        {
            if (tempPos.y >= 0)
            {
                tempPos.y = -49.5f;
                tempPos.x = tempPos.x - pixcel.transform.localScale.x;
                yield return new WaitForSeconds(0.01f);
            }
            if (tempPos.x <= -89)
                break;
            Vector2 temp = tempPos;
            // 깊이에 따른 offset 조정
            if (tempPos.y < -10)
            {
                float depthFactor = Mathf.InverseLerp(-20f, -10f, tempPos.y); // -49.5에서 -20 사이의 깊이에 따라 0에서 1 사이의 값을 계산
                float adjustedOffset = Mathf.Lerp(0, Random.Range(minOffst, maxOffst), depthFactor); // depthFactor에 따라 offset 조정
                temp.y += adjustedOffset;
            }
            else
            {
                temp.y += Random.Range(minOffst, maxOffst);
            }
            Instantiate(pixcel, temp, pixcel.transform.rotation);
            tempPos.y = tempPos.y + pixcel.transform.localScale.y;
            yield return null;
        }
    }
}
